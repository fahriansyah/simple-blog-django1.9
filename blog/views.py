# Created @JARVIS-desktop by fachryansyah
# https://facebook.com/fachry.ansyah.10
from django.shortcuts import render
from .models import Post

def index(request):
    post = Post.objects.all()
    return render(request, "blog/index.html", {"posts":post})

def detail(request, id):
    post = Post.objects.get(id=id)
    return render(request, "blog/detail.html", {'post':post})
