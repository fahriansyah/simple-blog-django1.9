# Created @JARVIS-desktop by fachryansyah
# https://facebook.com/fachry.ansyah.10
from django.contrib import admin
from .models import Post

admin.site.register(Post)
