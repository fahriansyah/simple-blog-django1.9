# Created @JARVIS-desktop by fachryansyah
# https://facebook.com/fachry.ansyah.10
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name = "index"),
    url(r'^post/(?P<id>\d+)$', views.detail, name = "post"),
]