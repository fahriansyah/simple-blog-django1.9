# Created @JARVIS-desktop by fachryansyah
# https://facebook.com/fachry.ansyah.10
from django.db import models

class Post(models.Model):
    title = models.CharField(max_length = 100)
    img = models.CharField(max_length = 100, null = True)
    content = models.TextField(null = True)
    created_by = models.CharField(max_length = 100)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

