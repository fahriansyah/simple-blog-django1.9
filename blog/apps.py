# Created @JARVIS-desktop by fachryansyah
# https://facebook.com/fachry.ansyah.10
from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'blog'
